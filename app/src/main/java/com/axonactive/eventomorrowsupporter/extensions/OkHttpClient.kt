package com.axonactive.eventomorrowsupporter.extensions

import android.content.Context
import com.axonactive.eventomorrowsupporter.appContext
import com.axonactive.eventomorrowsupporter.data.source.PreferenceDataSource
import com.axonactive.eventomorrowsupporter.data.source.local.UserLocalSource.Companion.PREFERENCE_ACCESS_TOKEN
import okhttp3.Credentials
import okhttp3.Headers
import okhttp3.OkHttpClient

fun OkHttpClient.Builder.addHeaderForBearerAuth(): OkHttpClient.Builder {
    addInterceptor { chain ->
        val header = appContext.getSharedPreferences(PreferenceDataSource.PREFERENCE_NAME, Context.MODE_PRIVATE).getString(PREFERENCE_ACCESS_TOKEN, "")
        val request = chain.request().newBuilder()
                .headers(Headers.of("Authorization", "Bearer $header",
                        "User-Agent", "android"))
                .build()
        chain.proceed(request)
    }
    return this
}

fun OkHttpClient.Builder.addHeaderForBasicAuth(id: String, password: String): OkHttpClient.Builder {
    return addInterceptor { chain ->
        val credential = Credentials.basic("$id", "$password")
        val request = chain.request().newBuilder()
                .headers(Headers.of("Authorization", credential,
                        "User-Agent", "android"))
                .build()
        chain.proceed(request)
    }
}