package com.axonactive.eventomorrowsupporter.extensions

import java.util.regex.Pattern

const val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
const val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$"
const val MOBILE_PATTERN = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
const val MIN_LENGTH_PASSWORD = 6
const val MIN_LENGTH_PHONE = 5
const val MAX_LENGTH_PHONE = 10
const val MIN_LENGTH_NAME = 2
const val PHONE_NUMBER_PATTERN = "[0-9]{9,11}\\z"

fun String.isValidEmail(): Boolean {
    val pattern = Pattern.compile(EMAIL_PATTERN)
    return pattern.matcher(this).matches()
}

fun String.isValidMobile(): Boolean {
    val pattern = Pattern.compile(MOBILE_PATTERN)
    return pattern.matcher(this).matches()
}

fun String.isValidPassword(): Boolean {
    val pattern = Pattern.compile(PASSWORD_PATTERN)
    return pattern.matcher(this).matches()
}

fun String.isValidPhoneNumber(): Boolean {
    val parttern = Pattern.compile(PHONE_NUMBER_PATTERN)
    return parttern.matcher(this).matches()
}


fun String.isMinLengthPassword(): Boolean = this.length < MIN_LENGTH_PASSWORD

fun String.isMinLengthName(): Boolean = this.length < MIN_LENGTH_NAME

fun String.isMinLengthPhone(): Boolean = this.length < MIN_LENGTH_PHONE

fun String.isMaxLengthPhone(): Boolean = this.length > MAX_LENGTH_PHONE