package com.axonactive.eventomorrowsupporter.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast

fun Context.isInternetAvailable(): Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun Context.showToast(data: String, type: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, data, type).show()
}
