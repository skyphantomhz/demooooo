package com.axonactive.eventomorrowsupporter.extensions

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.utils.ViewPortHandler
import com.github.mikephil.charting.formatter.IValueFormatter
import java.text.DecimalFormat

class ValueFormatChart: IValueFormatter {

    private val mFormat: DecimalFormat = DecimalFormat("###,###,##0")

    override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {

        return if (value > 0) {
            mFormat.format(value)
        } else {
            ""
        }
    }
}