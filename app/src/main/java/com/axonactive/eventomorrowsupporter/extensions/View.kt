package com.axonactive.eventomorrowsupporter.extensions

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.setHideKeyBoardListener(activity: FragmentActivity?) {
    this.setOnTouchListener { _, _ ->
        val imm = activity?.applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive)
            imm.hideSoftInputFromWindow(windowToken, 0)
        false
    }
}
