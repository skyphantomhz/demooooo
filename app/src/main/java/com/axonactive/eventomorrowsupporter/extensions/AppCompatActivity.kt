package com.axonactive.eventomorrowsupporter.extensions

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.axonactive.eventomorrowsupporter.R

fun AppCompatActivity.replaceFragmentInActivity(fragment: Fragment, @IdRes frameId: Int) {
    supportFragmentManager.transact {
        setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
        replace(frameId, fragment)
        addToBackStack(fragment::class.java.simpleName)
    }
}

fun AppCompatActivity.addFragmentToActivity(fragment: Fragment, tag: String) {
    supportFragmentManager.transact {
        add(fragment, tag)
    }
}

private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

fun AppCompatActivity.showDialogOnlyAccept(context: Context, title: String, content: String) {
    AlertDialog.Builder(context).create().apply {
        setTitle(title)
        setCancelable(false)
        setMessage(content)
        setButton(AlertDialog.BUTTON_POSITIVE, "OK", { _: DialogInterface, _: Int ->
            exitApplication(context)
        })
        show()
    }
}

private fun exitApplication(context: Context) {
    val homeIntent = Intent(Intent.ACTION_MAIN)
    homeIntent.addCategory(Intent.CATEGORY_HOME)
    homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    context.startActivity(homeIntent)
}
