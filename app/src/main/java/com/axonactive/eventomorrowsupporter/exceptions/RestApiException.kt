package com.axonactive.eventomorrowsupporter.exceptions


import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.base.BaseCallBack
import com.axonactive.eventomorrowsupporter.base.BaseException
import com.axonactive.eventomorrowsupporter.data.MessageCode
import com.axonactive.eventomorrowsupporter.getStringResource
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.NoRouteToHostException
import java.net.SocketTimeoutException

class RestApiException(override var error: Throwable) : BaseException {
    override var message: String? = null
    lateinit var callBack: CallBackError
    override fun onError(baseCallBack: BaseCallBack) {
        callBack = baseCallBack as CallBackError
        error.let {
            when (it) {
                is HttpException -> resolveErrorHandledOnRestApi(it)
                is ConnectException -> callBack.connectException()
                is NoRouteToHostException -> callBack.onNoRoute()
                is SocketTimeoutException -> message = getStringResource(R.string.string_time_out_exception)
                else -> {
                    message = getStringResource(R.string.string_exception_not_handle)
                }
            }
        }
    }

    private fun resolveErrorHandledOnRestApi(error: HttpException) {
        when (error.code()) {
            HttpURLConnection.HTTP_BAD_REQUEST -> {
                message = getBodyBadRequest(error)
            }
            HttpURLConnection.HTTP_UNAUTHORIZED -> {
                message = getStringResource(R.string.string_unAuthorized)
                callBack.onUnAuthorize()
            }
            HttpURLConnection.HTTP_FORBIDDEN -> {
                message = getStringResource(R.string.string_forbidden)
                callBack.onForBidden()
            }
            HttpURLConnection.HTTP_NOT_FOUND -> {
                message = getStringResource(R.string.string_not_found)
            }
            HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                message = getStringResource(R.string.string_not_found)
                callBack.onServerError()
            }
        }
    }

    private fun getBodyBadRequest(error: HttpException): String {
        error.response().errorBody()?.let {
            val bodyError = "${it.string()}"
            return when (bodyError) {
                MessageCode.TICKET_NOT_EXIST -> getStringResource(R.string.string_not_found)
                MessageCode.CANNOT_CHECK_IN_FOR_SUPPORTER -> getStringResource(R.string.string_check_in_supporter)
                MessageCode.USER_RECEIVED_GIFT -> getStringResource(R.string.string_user_recevived_git)
                MessageCode.CHECK_IN_TIME_INVALID -> getStringResource(R.string.string_check_in_time_invalid)
                MessageCode.TICKET_ALREADY_CHECKED_IN -> getStringResource(R.string.string_code_already_checked)
                else -> getStringResource(R.string.string_badRequest)
            }
        } ?: run {
            callBack.onBadRequest()
            return getStringResource(R.string.string_badRequest)
        }
    }

    interface CallBackError : BaseCallBack {
        fun connectException()
        fun onUnAuthorize()
        fun onBadRequest()
        fun onServerError()
        fun onForBidden()
        fun onNoRoute()
    }
}
