package com.axonactive.eventomorrowsupporter

import android.app.Activity
import android.app.Application
import android.support.multidex.MultiDex
import com.axonactive.eventomorrowsupporter.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


class MainApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    companion object {
        lateinit var application: MainApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        application = this
        DaggerAppComponent
                .builder()
                .application(this)
                .build().inject(this)
    }
}