package com.axonactive.eventomorrowsupporter

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater

class LoaderDialog constructor(context: Context) : Dialog(context, R.style.NewDialog) {

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_loader, null)
        setContentView(view)
    }
}