package com.axonactive.eventomorrowsupporter.di.module

import com.axonactive.eventomorrowsupporter.feature.main.analysis.AnalysisFragment
import com.axonactive.eventomorrowsupporter.feature.main.home.HomeFragment
import com.axonactive.eventomorrowsupporter.feature.main.scanner.ScannerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun bindHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun bindScannerFragment(): ScannerFragment

    @ContributesAndroidInjector
    abstract fun bindAnalysisFragment(): AnalysisFragment
}