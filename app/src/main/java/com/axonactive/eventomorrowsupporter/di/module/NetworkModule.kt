package com.axonactive.eventomorrowsupporter.di.module

import android.content.Context
import android.content.SharedPreferences
import com.axonactive.eventomorrowsupporter.BuildConfig
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.appContext
import com.axonactive.eventomorrowsupporter.data.service.AuthenticationService
import com.axonactive.eventomorrowsupporter.data.service.EventService
import com.axonactive.eventomorrowsupporter.data.service.ScanService
import com.axonactive.eventomorrowsupporter.data.source.*
import com.axonactive.eventomorrowsupporter.data.source.local.UserLocalSource
import com.axonactive.eventomorrowsupporter.data.source.remote.*
import com.axonactive.eventomorrowsupporter.extensions.addHeaderForBasicAuth
import com.axonactive.eventomorrowsupporter.extensions.addHeaderForBearerAuth
import com.axonactive.eventomorrowsupporter.getStringResource
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideDatabaseReference() = FirebaseDatabase.getInstance().getReference("events")

    @Provides
    @Singleton
    fun provideAnalysisRemoteSource(databaseReference: DatabaseReference) = AnalysisRemoteSource(databaseReference)

    @Provides
    @Singleton
    fun provideAnalysisDataSource(analysisRemoteSource: AnalysisRemoteSource) = AnalysisDataSource(analysisRemoteSource)


    @Provides
    @Singleton
    fun provideSharedPreference() = appContext.getSharedPreferences(PreferenceDataSource.PREFERENCE_NAME, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun providePreferenceRepository(sharedPreferences: SharedPreferences) = PreferenceDataSource(sharedPreferences)

    @Provides
    @Singleton
    @Named("okHttpclient-client")
    fun provideBasicAuthClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addHeaderForBasicAuth(getStringResource(R.string.app_id),
                        getStringResource(R.string.app_secret)).build()
    }


    @Provides
    @Singleton
    @Named("retrofit-client")
    fun createInstanceWithClientBasic(@Named("okHttpclient-client") httpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        val clientWithHeader = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build()

        return clientWithHeader
    }


    @Provides
    @Singleton
    @Named("auth-basic")
    fun provideAuthenticationServiceWithBasicAuth(@Named("retrofit-client") ApiClient: Retrofit) =
            ApiClient.create(AuthenticationService::class.java)

    @Provides
    fun provideUserLocalSource(sharedPreferences: SharedPreferences) = UserLocalSource(sharedPreferences)

    @Provides
    @Singleton
    fun provideUserRemoteSource(@Named("auth-basic") authenticationService: AuthenticationService
                                , userLocalSource: UserLocalSource) =
            UserRemoteSource(userLocalSource, authenticationService)

    @Provides
    fun provideUserSource(userLocal: UserLocalSource, userRemote: UserRemoteSource)
            = UserSource(userRemote, userLocal)

    @Provides
    @Named("okHttpclient-client_auth")
    fun provideBearerAuthClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addHeaderForBearerAuth().build()
    }

    @Provides
    @Named("retrofit-client-bearer")
    fun createInstanceWithClient(@Named("okHttpclient-client_auth") httpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        val clientWithHeader = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build()

        return clientWithHeader
    }

    @Provides
    @Named("auth-bearer")
    fun provideAuthenticationServiceWithBearerAuth(@Named("retrofit-client-bearer") ApiClient: Retrofit) =
            ApiClient.create(AuthenticationService::class.java)

    @Provides
    fun provideUserRemoteSourceAuth(@Named("auth-bearer") authenticationServiceBearer: AuthenticationService,
                                    userLocalSource: UserLocalSource) =
            UserRemoteSourceAuth(userLocalSource, authenticationServiceBearer)

    @Provides
    @Singleton
    fun provideScanService(@Named("retrofit-client-bearer") retrofit: Retrofit): ScanService {
        return retrofit.create(ScanService::class.java)
    }

    @Provides
    fun provideScanRemoteSource(scanService: ScanService) = ScanRemoteSource(scanService)

    @Provides
    @Singleton
    fun provideScanDataSource(scanRemoteSource: ScanRemoteSource) = ScanDataSource(scanRemoteSource)

    @Provides
    @Singleton
    fun provideEventService(@Named("retrofit-client-bearer") retrofit: Retrofit): EventService {
        return retrofit.create(EventService::class.java)
    }

    @Provides
    @Singleton
    fun getEventRemoteSource(eventService: EventService) = EventRemoteSource(eventService)

    @Provides
    @Singleton
    fun getEventDataSource(eventRemoteSource: EventRemoteSource) = EventDataSource(eventRemoteSource)
}