package com.axonactive.eventomorrowsupporter.di.module

import com.axonactive.eventomorrowsupporter.feature.onboarding.login.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginActivityModule {

    @ContributesAndroidInjector
    abstract fun bindLoginFragment(): LoginFragment
}
