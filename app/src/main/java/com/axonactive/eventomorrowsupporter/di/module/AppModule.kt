package com.axonactive.eventomorrowsupporter.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import javax.inject.Singleton
import dagger.Provides



@Module
class AppModule{
    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }
}