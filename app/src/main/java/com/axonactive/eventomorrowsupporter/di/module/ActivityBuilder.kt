package com.axonactive.eventomorrowsupporter.di.module

import com.axonactive.eventomorrowsupporter.feature.MainActivity
import com.axonactive.eventomorrowsupporter.feature.onboarding.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [(LoginActivityModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun contributeMainActivity(): MainActivity
}