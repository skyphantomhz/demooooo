package com.axonactive.eventomorrowsupporter

import com.axonactive.eventomorrowsupporter.base.BaseException
import com.axonactive.eventomorrowsupporter.exceptions.RestApiException
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

open class SimpleSubscribe<T> : Observer<T> {
    private var error: BaseException? = null
    private var value: T? = null

    override fun onSubscribe(d: Disposable) {

    }

    override fun onComplete() {
        if (error != null) {
            onCompleted(false, value, error)
            return
        }
        onCompleted(true, value, null)
    }

    open fun onCompleted(success: Boolean, value: T?, error: BaseException?) {

    }

    override fun onNext(value: T) {
        this.value = value
    }

    override fun onError(error: Throwable) {
        this.error = RestApiException(error)
        onComplete()
    }
}