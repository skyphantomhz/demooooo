package com.axonactive.eventomorrowsupporter.feature.main.home

class HomePresenter(val view: HomeContract.View) : HomeContract.Presenter {

    init {
        view.presenter = this
    }

    override fun switchToScan() {
        view.switchToScan()
    }

    override fun refreshView(value: String) {
        view.showResponse(value)
    }

    override fun showInformationCheckin(error: String) {
        view.showInformationCheckin(error)
    }
}