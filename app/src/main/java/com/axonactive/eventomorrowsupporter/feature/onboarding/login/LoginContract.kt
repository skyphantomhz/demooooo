package com.axonactive.eventomorrowsupporter.feature.onboarding.login

import com.axonactive.eventomorrowsupporter.base.BaseView
import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.source.PreferenceDataSource
import com.axonactive.eventomorrowsupporter.data.source.UserSource
import com.axonactive.eventomorrowsupporter.data.source.remote.UserRemoteSourceAuth


interface LoginContract {

    interface View : BaseView<Presenter> {
        fun showDiaLog(title: String, content: String)
        fun loginSuccess(email: String?)
        fun loginError(error: String)
        fun moveToHomePage()
        fun getUserSource(): UserSource
        fun getPreferenceSource(): PreferenceDataSource
        fun getUserReSourceAuth(): UserRemoteSourceAuth
    }

    interface Presenter {

        fun loginByEmail(email: String, password: String)
        fun loginByFacebook(token: String)
        fun loginByGoogle(token: String)
        fun saveUserAccessToken(token: String)
        fun saveUserRefreshToken(token: String)
        fun saveUserProfile(profile: UserProfile)
    }
}