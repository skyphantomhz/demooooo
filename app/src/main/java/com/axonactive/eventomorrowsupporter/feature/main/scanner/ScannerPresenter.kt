package com.axonactive.eventomorrowsupporter.feature.main.scanner

import android.util.Log
import com.axonactive.eventomorrowsupporter.data.MessageCode
import com.google.zxing.Result

class ScannerPresenter(val view: ScannerContract.View) : ScannerContract.Presenter {
    init {
        view.presenter = this
    }

    override fun handleResultQrCode(rawResult: Result?) {
        rawResult?.let {
            val codeFormat = rawResult.barcodeFormat.toString()
            when (codeFormat) {
                MessageCode.CODE_FORMAT_FOR_SCAN -> qrCodeScanOk(rawResult)
            }
        } ?: run {
            Log.d("TAG", "hello") // Prints scan results
        }
    }

    private fun qrCodeScanOk(rawResult: Result) {
        view.switchToHome(rawResult)
    }


}