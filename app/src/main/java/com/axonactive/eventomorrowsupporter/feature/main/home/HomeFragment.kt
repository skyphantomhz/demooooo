package com.axonactive.eventomorrowsupporter.feature.main.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.SimpleSubscribe
import com.axonactive.eventomorrowsupporter.appContext
import com.axonactive.eventomorrowsupporter.base.BaseException
import com.axonactive.eventomorrowsupporter.base.BaseFragment
import com.axonactive.eventomorrowsupporter.data.model.Event
import com.axonactive.eventomorrowsupporter.data.model.response.EventResponse
import com.axonactive.eventomorrowsupporter.data.source.EventDataSource
import com.axonactive.eventomorrowsupporter.exceptions.RestApiException
import com.axonactive.eventomorrowsupporter.feature.MainActivity
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeContract.View {

    override lateinit var presenter: HomeContract.Presenter
    private var isScanGiftCode = false
    @Inject lateinit var eventDataSource: EventDataSource
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).events?.let {
            initSpinnerEvents(it)
        } ?: run {
            (activity as MainActivity).showLoading()
            getListEventsUpcomming()
        }
        btn_show_scan.setOnClickListener {
            tv_information_ticket.text = ""
            presenter.switchToScan()
        }
        setupSwitchButton()
    }

    private fun getListEventsUpcomming() {
        val observer = object : SimpleSubscribe<EventResponse>() {
            override fun onCompleted(success: Boolean, value: EventResponse?, error: BaseException?) {
                super.onCompleted(success, value, error)
                when {
                    success && value != null -> value.listEvents?.let {
                        initSpinnerEvents(it)
                        (activity as MainActivity).events = it
                    }
                    else -> handleExceptionOnRestApi(error!! as RestApiException)
                }
            }
        }
        eventDataSource.getUpComingEvents(0, 10).subscribe(observer)
    }

    private fun handleExceptionOnRestApi(restApiException: RestApiException) {
        restApiException.onError(object : RestApiException.CallBackError {
            override fun onUnAuthorize() {
                (activity as MainActivity).logout()
            }

            override fun onBadRequest() {}

            override fun onServerError() {}

            override fun onForBidden() {}

            override fun onNoRoute() {}

            override fun connectException() {
                (activity as MainActivity).detectInternetState()
            }
        })
        restApiException.message?.let { showErrorByToast(it) }
    }

    fun initSpinnerEvents(value: List<Event>) {
        val adapter = ArrayAdapter<Event>(activity, R.layout.item_spinner_event,
                value)
        spinner_select_event_home.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                (activity as MainActivity).eventId = value[position].id
                (activity as MainActivity).position = position
            }
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_select_event_home.adapter = adapter
        (activity as MainActivity).position?.let {
            spinner_select_event_home.setSelection(it)
        }
        (activity as MainActivity).cancelLoading()
    }

    override fun showErrorByToast(error: String) {
        Toast.makeText(appContext, error, Toast.LENGTH_SHORT).show()
    }

    override fun showResponse(text: String) {
        setTextView(text)
    }

    private fun setupSwitchButton() {
        isScanGiftCode = (activity as MainActivity).scanGiftCode
        switch_gift_code.isChecked = isScanGiftCode
        switch_gift_code.setOnCheckedChangeListener { _, isChecked ->
            (activity as MainActivity).updateTypeScan(isChecked)
            isScanGiftCode = isChecked
        }
    }

    private fun setTextView(message: String) {
        tv_information_ticket.text = message
        tv_information_ticket.visibility = View.VISIBLE
    }

    override fun showInformationCheckin(error: String) {
        setTextView(error)
    }

    override fun switchToScan() {
        (activity as MainActivity).switchToScan()
    }
}