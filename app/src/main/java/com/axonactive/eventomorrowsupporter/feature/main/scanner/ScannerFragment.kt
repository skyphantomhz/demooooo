package com.axonactive.eventomorrowsupporter.feature.main.scanner

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.axonactive.eventomorrowsupporter.base.BaseFragment
import com.axonactive.eventomorrowsupporter.feature.MainActivity
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScannerFragment : BaseFragment(), ScannerContract.View, ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
    override lateinit var presenter: ScannerContract.Presenter

    companion object {
        val MY_PERMISSIONS_REQUEST_CAMERA = 2303
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mScannerView = ZXingScannerView(activity)
        return mScannerView
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }

    override fun handleResult(rawResult: Result?) {
        presenter.handleResultQrCode(rawResult)
    }

    override fun onResume() {
        super.onResume()
        mScannerView?.setResultHandler(this)
        mScannerView?.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()
    }

    @SuppressLint("SetTextI18n")
    override fun showToast(rawResult: Result) {
        mScannerView?.resumeCameraPreview(this)
    }

    override fun switchToHome(rawResult: Result) {
        (activity as MainActivity).comeBackHome(rawResult)
    }
}
