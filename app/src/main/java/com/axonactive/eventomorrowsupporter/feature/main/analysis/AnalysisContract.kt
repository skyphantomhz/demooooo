package com.axonactive.eventomorrowsupporter.feature.main.analysis

import com.axonactive.eventomorrowsupporter.base.BaseView

interface AnalysisContract {
    interface View: BaseView<Presenter>
    interface Presenter
}