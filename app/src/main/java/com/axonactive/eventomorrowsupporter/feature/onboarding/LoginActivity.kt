package com.axonactive.eventomorrowsupporter.feature.onboarding

import android.content.Intent
import android.os.Bundle
import com.axonactive.eventomorrowsupporter.HasSupportFragmentInjectorActivity
import com.axonactive.eventomorrowsupporter.LoaderDialog
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.data.source.UserSource
import com.axonactive.eventomorrowsupporter.extensions.replaceFragmentInActivity
import com.axonactive.eventomorrowsupporter.extensions.showDialogOnlyAccept
import com.axonactive.eventomorrowsupporter.feature.MainActivity
import com.axonactive.eventomorrowsupporter.feature.onboarding.login.LoginFragment
import com.axonactive.eventomorrowsupporter.feature.onboarding.login.LoginPresenter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import javax.inject.Inject

class LoginActivity : HasSupportFragmentInjectorActivity(), LoginFragment.ActivityListener {

    @Inject lateinit var userSource: UserSource
    private val loginFragment = LoginFragment.instance
    private lateinit var loaderDialog: LoaderDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        moveToLoginPage()
        isAccessTokenExisted()
        initPresenter()
        loaderDialog = LoaderDialog(this)
    }


    override fun getLayoutResourceId() = R.layout.activity_login

    private fun initPresenter() {
        LoginPresenter(loginFragment)
    }

    private fun isAccessTokenExisted() {
        if (!userSource.isTokenEmpty()) {
            moveToHomePage()
        }
    }

    fun moveToLoginPage() {
        replaceFragmentInActivity(loginFragment, R.id.frame_main_view)
    }

    override fun moveToHomePage() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showLoading() {
        if (!loaderDialog.isShowing) {
            loaderDialog.show()
        }
    }

    override fun cancelLoading() {
        if (loaderDialog.isShowing) {
            loaderDialog.dismiss()
        }
    }

    override fun loginGoogle(intent: Intent) {
        startActivityForResult(intent, LoginFragment.RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.frame_main_view)
        (fragment as? LoginFragment)?.onActivityResult(requestCode, resultCode, data)
    }

    override fun showDialog(title: String, content: String) {
        showDialogOnlyAccept(this, title, content)
        cancelLoading()
    }

    override fun loginGoogleError() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestIdToken(getString(R.string.server_client_id))
                .build()
        GoogleSignIn.getClient(this, gso).signOut()
    }

//    override fun onDestroy() {
//        super.onDestroy()
////        UserSource.destroyInstance()
//    }

}
