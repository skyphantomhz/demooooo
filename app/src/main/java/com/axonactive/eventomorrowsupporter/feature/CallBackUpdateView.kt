package com.axonactive.eventomorrowsupporter.feature

import com.axonactive.eventomorrowsupporter.base.BaseCallBack
import com.google.firebase.database.DatabaseError

interface CallBackUpdateView: BaseCallBack {
    fun updateView(unconfirmed: Int, confirmed: Int,checkIn: Int, declined: Int )
    fun onCancelledFirebase(databaseError: DatabaseError)
}