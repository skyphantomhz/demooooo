package com.axonactive.eventomorrowsupporter.feature.main.analysis

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.axonactive.eventomorrowsupporter.LoaderDialog
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.base.BaseFragment
import com.axonactive.eventomorrowsupporter.data.AnalyticsStatus
import com.axonactive.eventomorrowsupporter.data.model.Event
import com.axonactive.eventomorrowsupporter.data.source.AnalysisDataSource
import com.axonactive.eventomorrowsupporter.extensions.ValueFormatChart
import com.axonactive.eventomorrowsupporter.feature.CallBackUpdateView
import com.axonactive.eventomorrowsupporter.feature.MainActivity
import com.axonactive.eventomorrowsupporter.getColorResource
import com.axonactive.eventomorrowsupporter.getDimen
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.firebase.database.DatabaseError
import kotlinx.android.synthetic.main.fragment_analysis.*
import java.util.*
import javax.inject.Inject

class AnalysisFragment : BaseFragment(), AnalysisContract.View, CallBackUpdateView {
    override lateinit var presenter: AnalysisContract.Presenter
    @Inject lateinit var analysisDataSource: AnalysisDataSource
    private var formData: HashMap<String, Float> = hashMapOf(AnalyticsStatus.status[confirmedPosition] to 0f
            , AnalyticsStatus.status[unconfirmedPosition] to 0f, AnalyticsStatus.status[declinedPosition] to 0f
            , AnalyticsStatus.status[checkInPosition] to 0f)
    private var listData = ArrayList<PieEntry>()
    private lateinit var pieData: PieData
    private lateinit var pieDataSet: PieDataSet
    private lateinit var loaderDialog: LoaderDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_analysis, container, false)
    }

    private fun initSpinnerEvents(value: List<Event>) {
        val adapter = ArrayAdapter<Event>(activity, R.layout.item_spinner_event,
                value)
        spinner_select_event.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                analysisDataSource.setEventId(value[position].id)
                (activity as MainActivity).showLoading()
                getEventsSuccess(position, value[position].id)
            }

        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_select_event.adapter = adapter
        (activity as MainActivity).position?.let {
            spinner_select_event.setSelection(it)
            analysisDataSource.setEventId(it)
        }
    }

    private fun getEventsSuccess(position: Int, eventId: Int) {
        (activity as MainActivity).let {
            it.position = position
            it.eventId = eventId
        }
    }

    companion object {
        val textSizeSmall = getDimen(R.dimen.chart_text_size_small)
        val textSizeTini = getDimen(R.dimen.chart_text_size_tini)
        val textSizeMedium = getDimen(R.dimen.chart_text_size_medium)
        val colorBlue = getColorResource(R.color.colorBlue)
        const val confirmedPosition = 0
        const val unconfirmedPosition = 1
        const val declinedPosition = 2
        const val checkInPosition = 3
    }

    private fun setupPieChart() {
        setupDataChar()
        setupLegendChart()
        setupDescriptionChar()
        chart_analysis.setDrawEntryLabels(false)
        chart_analysis.invalidate()
    }

    private fun setupDataChar() {
        for (i in 0..3) {
            formData[AnalyticsStatus.status[i]]?.let {
                listData.add(PieEntry(it, AnalyticsStatus.status[i]))
            }

        }
        pieDataSet = PieDataSet(listData, "")
        pieDataSet.valueTextSize = textSizeSmall
        pieDataSet.valueTextColor = getColorResource(R.color.colorWhite)
        pieDataSet.colors = ColorTemplate.MATERIAL_COLORS.toCollection(ArrayList())
        pieData = PieData(pieDataSet)
        chart_analysis.data = pieData
        pieData.setValueFormatter(ValueFormatChart())
    }

    private fun setupLegendChart() {
        chart_analysis.legend.let {
            it.formSize = getDimen(R.dimen.legend_form_size)
            it.form = Legend.LegendForm.CIRCLE
            it.textSize = textSizeTini
            it.textColor = colorBlue
        }
    }

    private fun setupDescriptionChar() {
        chart_analysis.setCenterTextColor(colorBlue)
        chart_analysis.setCenterTextSize(textSizeMedium)
        chart_analysis.description = Description().apply { text = "" }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.let {
            loaderDialog = LoaderDialog(activity!!)
        }
        (activity as MainActivity).events?.let {
            initSpinnerEvents(it)
        }
        super.onViewCreated(view, savedInstanceState)
        analysisDataSource.setListenerUpdateView(this)
        setupPieChart()

    }

    override fun updateView(unconfirmed: Int, confirmed: Int, checkIn: Int, declined: Int) {
        listData[checkInPosition] = PieEntry(checkIn.toFloat(), AnalyticsStatus.status[checkInPosition])
        listData[unconfirmedPosition] = PieEntry(unconfirmed.toFloat(), AnalyticsStatus.status[unconfirmedPosition])
        listData[confirmedPosition] = PieEntry(confirmed.toFloat(), AnalyticsStatus.status[confirmedPosition])
        listData[declinedPosition] = PieEntry(declined.toFloat(), AnalyticsStatus.status[declinedPosition])
        refreshView()
        (activity as MainActivity).cancelLoading()
    }

    private fun refreshView() {
        pieData.notifyDataChanged()
        chart_analysis?.let {
            it.notifyDataSetChanged()
            it.invalidate()
        }
        chart_analysis.centerText = "${getString(R.string.string_text_center_pie_chart)} ${analysisDataSource.participantCount()}"
    }

    override fun onCancelledFirebase(databaseError: DatabaseError) {
        val messageFail = getString(R.string.string_toast_fail_read_data)
        Toast.makeText(activity, "$messageFail $databaseError", Toast.LENGTH_LONG).show()
    }

    override fun onDetach() {
        analysisDataSource.remoteListenerUpdateView()
        super.onDetach()
    }
}