package com.axonactive.eventomorrowsupporter.feature.onboarding.login


import android.app.Activity.RESULT_CANCELED
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.base.BaseFragment
import com.axonactive.eventomorrowsupporter.data.source.PreferenceDataSource
import com.axonactive.eventomorrowsupporter.data.source.UserSource
import com.axonactive.eventomorrowsupporter.data.source.remote.UserRemoteSourceAuth
import com.axonactive.eventomorrowsupporter.extensions.*
import com.axonactive.eventomorrowsupporter.feature.onboarding.LoginActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.api.GoogleApiClient
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*
import javax.inject.Inject


class LoginFragment : BaseFragment(), LoginContract.View {

    override lateinit var presenter: LoginContract.Presenter
    private lateinit var callBackManager: CallbackManager
    private lateinit var googleApiClient: GoogleApiClient
    private lateinit var listener: ActivityListener
    private lateinit var gso: GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient
    @Inject lateinit var userRepository: UserSource
    @Inject lateinit var userRemoteSourceAuth: UserRemoteSourceAuth
    @Inject lateinit var preferenceRepository: PreferenceDataSource

    override fun getUserSource() = userRepository

    override fun getPreferenceSource() = preferenceRepository

    override fun getUserReSourceAuth() = userRemoteSourceAuth

    interface ActivityListener {
        fun moveToHomePage()
        fun showLoading()
        fun showDialog(title: String, content: String)
        fun cancelLoading()
        fun loginGoogleError()
        fun loginGoogle(intent: Intent)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? LoginActivity ?: throw Exception("Your activity must be implemented interface ActivityListener")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    private fun initGoogleSignIn() {
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestIdToken(getString(R.string.server_client_id))
                .build()
        this@LoginFragment.context?.let {
            //            googleApiClient = GoogleApiClient.Builder(it)
//                    .enableAutoManage(activity!!, { loginError(it.errorMessage!!) })
//                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                    .build()
            googleSignInClient = GoogleSignIn.getClient(it, gso)
        }
    }

    override fun onPause() {
        super.onPause()
//        googleApiClient.stopAutoManage(activity!!)
//        googleApiClient.disconnect()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_login.setHideKeyBoardListener(activity!!)
        initListener()
        initFacebookLogin()
        initGoogleSignIn()
        btn_facebook.fragment = this
    }

    private fun initListener() {
        initEmailTextChangeListener()
        initPasswordListener()
        initButtonSignInListener()
        initButtonSignInFacebookListener()
        initButtonSignInGoogleListener()
    }

    private fun initPasswordListener() {
        edt_login_password.afterTextChanged {
            if (it.isEmpty()) {
                tv_error_account.visibility = View.INVISIBLE
            } else {
                if (it.length < MIN_LENGTH_PASSWORD) {
                    tv_error_account.text = getString(R.string.string_password_too_weak)
                    tv_error_account.visibility = View.VISIBLE
                } else {
                    tv_error_account.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun initButtonSignInGoogleListener() {
        btn_sign_in_google.setOnClickListener {
            val intent = googleSignInClient.signInIntent
//            val intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
            listener.loginGoogle(intent)

        }
    }

    private fun initButtonSignInFacebookListener() {
        btn_sign_in_facebook.setOnClickListener {
            showLoader()
            btn_facebook.performClick()
        }
    }

    private fun initButtonSignInListener() {
        btn_signin.setOnClickListener {

            when {
                checkFieldIsEmpty() -> context?.showToast(context?.resources?.getString(R.string.string_fill_all_field)!!)
                !edt_login_email.text.toString().isValidEmail() -> {
                    tv_error_email.text = getString(R.string.string_error_email)
                    tv_error_email.visibility = View.VISIBLE
                }
                edt_login_password.text.toString().length < MIN_LENGTH_PASSWORD -> {
                    return@setOnClickListener
                }
                else -> {
                    tv_error_account.visibility = View.INVISIBLE
                    showLoader()
                    presenter.loginByEmail(edt_login_email.text.toString(), edt_login_password.text.toString())
                }
            }
        }
    }

    private fun initEmailTextChangeListener() {
        edt_login_email.afterTextChanged {
            if (it.isNotEmpty()) {
                if (!it.isValidEmail()) {
                    tv_error_email.text = getString(R.string.string_error_email)
                    tv_error_email.visibility = View.VISIBLE
                    if (tv_error_account.text.isNotEmpty())
                        tv_error_account.visibility = View.INVISIBLE
                } else {
                    tv_error_email.visibility = View.INVISIBLE
                }
            } else {
                tv_error_email.visibility = View.INVISIBLE
                tv_error_account.visibility = View.INVISIBLE
            }
        }
    }

    private fun initFacebookLogin() {
        callBackManager = CallbackManager.Factory.create()
        val permissionNeeds = Arrays.asList("email")

        btn_facebook.registerCallback(callBackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                presenter.loginByFacebook(result?.accessToken?.token!!)
            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException?) {
                loginError(error.toString())
            }
        })
        btn_facebook.setReadPermissions(permissionNeeds)
    }

    private fun checkFieldIsEmpty(): Boolean = edt_login_email.text.isEmpty() || edt_login_password.text.isEmpty()

    private fun showLoader() {
        listener.showLoading()
    }

    private fun cancelLoader() {
        listener.cancelLoading()
    }

    override fun loginSuccess(email: String?) {
        cancelLoader()
        context?.showToast(getString(R.string.string_login_successfully) + " $email")
        listener.moveToHomePage()
    }

    override fun loginError(error: String) {
        tv_error_account.text = error
        tv_error_account.visibility = View.VISIBLE
        if (error == getString(R.string.string_forbidden)) {
            listener.loginGoogleError()
        }

        cancelLoader()
    }

    override fun moveToHomePage() {
        listener.moveToHomePage()
    }

    override fun showDiaLog(title: String, content: String) {
        listener.showDialog(title, content)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("google", "$requestCode , $resultCode ")
        if (requestCode == FacebookSdk.getCallbackRequestCodeOffset())
            callBackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleGoogleSignIn(task)
        }
        if (resultCode == RESULT_CANCELED) {
            cancelLoader()
        }
    }

    private fun clearData() {
        edt_login_email.setText("")
        edt_login_password.setText("")
    }

    private fun handleGoogleSignIn(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val account = result.signInAccount
            presenter.loginByGoogle(account?.idToken!!)
        } else {
            when (result.status.statusCode) {

                GoogleSignInStatusCodes.NETWORK_ERROR, GoogleSignInStatusCodes.INTERNAL_ERROR -> {
                    loginError(getString(R.string.string_login_failed))
                }
                else -> cancelLoader()
            }

        }
    }

    companion object {
        val instance = LoginFragment()
        val TAG: String = LoginFragment::class.java.simpleName
        const val RC_SIGN_IN = 7
    }
}