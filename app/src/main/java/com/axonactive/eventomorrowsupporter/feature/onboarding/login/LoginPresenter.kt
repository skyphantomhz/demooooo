package com.axonactive.eventomorrowsupporter.feature.onboarding.login

import android.util.Log
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.SimpleSubscribe
import com.axonactive.eventomorrowsupporter.base.BaseException
import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import com.axonactive.eventomorrowsupporter.data.source.PreferenceDataSource
import com.axonactive.eventomorrowsupporter.exceptions.RestApiException
import com.axonactive.eventomorrowsupporter.getStringResource


class LoginPresenter(private val view: LoginContract.View) : LoginContract.Presenter {

    init {
        view.presenter = this
    }

    val getProfileObserver = object : SimpleSubscribe<UserProfile>() {
        override fun onCompleted(success: Boolean, value: UserProfile?, error: BaseException?) {
            super.onCompleted(success, value, error)
            if (!success) {
                if (error is RestApiException)
                    handleError(error)
                else view.loginError(error?.message!!)
            }
        }
    }

    override fun loginByEmail(email: String, password: String) {
        val observer = object : SimpleSubscribe<AccessTokenResponse>() {
            override fun onCompleted(success: Boolean, value: AccessTokenResponse?, error: BaseException?) {
                super.onCompleted(success, value, error)
                if (!success) {
                    if (error is RestApiException)
                        handleError(error)
                    else view.loginError(error?.message!!)
                }
            }
        }

        view.getUserSource().loginByEmail(email, password)
                .doOnNext { tokenResponse: AccessTokenResponse ->
                    view.getUserReSourceAuth().fetchUserProfile()
                            .doOnNext { profile: UserProfile ->
                                view.loginSuccess(profile.email)
                                view.getPreferenceSource().saveLoginMode(PreferenceDataSource.LOGIN_MODE.NORMAL)
                            }
                            .subscribe(getProfileObserver)
                }
                .subscribe(observer)
    }

    private fun handleError(error: RestApiException) {
        error.onError(object : RestApiException.CallBackError {
            override fun onUnAuthorize() {
                view.loginError(error.message!!)
            }


            override fun connectException() {
                view.showDiaLog(getStringResource(R.string.string_title_no_internet)
                        , getStringResource(R.string.string_content_no_internet))
            }

            override fun onBadRequest() {
                //Not required
            }

            override fun onServerError() {
                view.loginError(error.message!!)
            }

            override fun onForBidden() {
                view.loginError(getStringResource(R.string.string_forbidden))
            }

            override fun onNoRoute() {
                view.loginError(getStringResource(R.string.No_Route))
            }
        })
    }

    override fun saveUserAccessToken(token: String) {
        view.getUserSource().saveAccessToken(token)
    }

    override fun saveUserRefreshToken(token: String) {
        view.getUserSource().saveRefreshToken(token)
    }

    override fun saveUserProfile(profile: UserProfile) {
        view.getUserSource().saveUserProfile(profile)
    }

    override fun loginByFacebook(token: String) {
        val observer = object : SimpleSubscribe<AccessTokenResponse>() {
            override fun onCompleted(success: Boolean, value: AccessTokenResponse?, error: BaseException?) {
                super.onCompleted(success, value, error)
                if (!success) {
                    if (error is RestApiException)
                        handleError(error)
                    else Log.e(TAG, error?.error?.localizedMessage)
                }
            }
        }
        view.getUserSource().loginByFacebook(token)
                .doOnNext { token: AccessTokenResponse ->
                    view.getUserReSourceAuth().fetchUserProfile()
                            .doOnNext { profile: UserProfile ->
                                view.getPreferenceSource().saveLoginMode(PreferenceDataSource.LOGIN_MODE.FACEBOOK)
                                view.loginSuccess(profile.email)
                            }
                            .subscribe(getProfileObserver)
                }
                .subscribe(observer)
    }

    override fun loginByGoogle(token: String) {
        Log.d("token", "token google $token")
        val observer = object : SimpleSubscribe<AccessTokenResponse>() {
            override fun onCompleted(success: Boolean, value: AccessTokenResponse?, error: BaseException?) {
                super.onCompleted(success, value, error)
                if (!success) {
                    if (error is RestApiException)
                        handleError(error)
                    else Log.e(TAG, error?.error?.localizedMessage)
                }
            }
        }

        view.getUserSource().loginByGoogle(token)
                .doOnNext { tokenResponse: AccessTokenResponse ->
                    view.getUserReSourceAuth().fetchUserProfile()
                            .doOnNext { profile: UserProfile ->
                                view.getPreferenceSource().saveLoginMode(PreferenceDataSource.LOGIN_MODE.GOOGLE)
                                view.loginSuccess(profile.email)
                            }
                            .subscribe(getProfileObserver)
                }
                .subscribe(observer)
    }

    companion object {
        val TAG: String = LoginPresenter::class.java.simpleName
        val UNAUTHORIZED_MESSAGE = "Bad credentials"
    }
}