package com.axonactive.eventomorrowsupporter.feature

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.axonactive.eventomorrowsupporter.*
import com.axonactive.eventomorrowsupporter.base.BaseException
import com.axonactive.eventomorrowsupporter.data.MessageCode
import com.axonactive.eventomorrowsupporter.data.model.Event
import com.axonactive.eventomorrowsupporter.data.model.ScanResponse
import com.axonactive.eventomorrowsupporter.data.source.ScanDataSource
import com.axonactive.eventomorrowsupporter.data.source.UserSource
import com.axonactive.eventomorrowsupporter.exceptions.RestApiException
import com.axonactive.eventomorrowsupporter.extensions.replaceFragmentInActivity
import com.axonactive.eventomorrowsupporter.feature.main.analysis.AnalysisContract
import com.axonactive.eventomorrowsupporter.feature.main.analysis.AnalysisFragment
import com.axonactive.eventomorrowsupporter.feature.main.analysis.AnalysisPresenter
import com.axonactive.eventomorrowsupporter.feature.main.home.HomeContract
import com.axonactive.eventomorrowsupporter.feature.main.home.HomeFragment
import com.axonactive.eventomorrowsupporter.feature.main.home.HomePresenter
import com.axonactive.eventomorrowsupporter.feature.main.scanner.ScannerContract
import com.axonactive.eventomorrowsupporter.feature.main.scanner.ScannerFragment
import com.axonactive.eventomorrowsupporter.feature.main.scanner.ScannerPresenter
import com.axonactive.eventomorrowsupporter.feature.onboarding.LoginActivity
import com.facebook.login.LoginManager
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import okhttp3.ResponseBody
import javax.inject.Inject

class MainActivity : HasSupportFragmentInjectorActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun getLayoutResourceId() = R.layout.activity_main
    private lateinit var scannerPresenter: ScannerContract.Presenter
    private lateinit var homePresenter: HomeContract.Presenter
    private lateinit var analysisPresenter: AnalysisContract.Presenter
    private lateinit var loaderDialog: LoaderDialog
    private lateinit var toggle: ActionBarDrawerToggle
    private var mToolBarNavigationListenerIsRegistered = false
    var scanGiftCode = false
    var eventId: Int? = null
    var position: Int? = null
    var events: List<Event>? = null

    @Inject
    lateinit var scanRepository: ScanDataSource
    @Inject lateinit var userSource: UserSource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loaderDialog = LoaderDialog(this)
        setSupportActionBar(toolbar)
        toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        switchToHome()
        nav_view.setNavigationItemSelectedListener(this)
        setCheckedInNav(0)
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
        if (permission == PackageManager.PERMISSION_DENIED) {
            makeRequest()
        } else if (permission == PackageManager.PERMISSION_GRANTED) {

        }
    }

    override fun onBackPressed() {
        Log.e("TAG", "back stack: ${supportFragmentManager.backStackEntryCount}")
        when {
            drawer_layout.isDrawerOpen(GravityCompat.START) ->
                drawer_layout.closeDrawer(GravityCompat.START)
            supportFragmentManager.backStackEntryCount <= 1 -> {
                super.onBackPressed()
            }
            else -> {
                supportFragmentManager.popBackStack()
                setCheckedInNav(0)
                enableViews(false)
            }
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
                ScannerFragment.MY_PERMISSIONS_REQUEST_CAMERA)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ScannerFragment.MY_PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish()
            }
        }
    }

    fun updateTypeScan(isScanGiftCode: Boolean) {
        scanGiftCode = isScanGiftCode
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_scan -> {
                when {
                    supportFragmentManager.backStackEntryCount > 1 -> {
                        enableViews(false)
                        supportFragmentManager.popBackStack()
                    }
                    supportFragmentManager.backStackEntryCount == 1 -> true
                    else -> switchToHome()
                }
            }
            R.id.nav_analytic -> {
                if (supportFragmentManager.backStackEntryCount == 1) switchToAnalysis()
            }
            R.id.nav_logout -> logout()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun logout() {
        LoginManager.getInstance()?.logOut()
        userSource.clearData()
        val intent = Intent(this, LoginActivity::class.java).apply {
            this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        startActivity(intent)
        finish()
    }

    fun switchToScan() {
        enableViews(true)
        ScannerFragment().let {
            scannerPresenter = ScannerPresenter(it)
            replaceFragmentInActivity(it, R.id.frame_content)
        }
    }

    private fun switchToAnalysis() {
        AnalysisFragment().let {
            analysisPresenter = AnalysisPresenter(it)
            replaceFragmentInActivity(it, R.id.frame_content)
        }
    }

    private fun switchToHome() {
        HomeFragment().let {
            homePresenter = HomePresenter(it)
            replaceFragmentInActivity(it, R.id.frame_content)
        }
    }

    fun comeBackHome(rawResult: Result) {
        showLoading()
        enableViews(false)
        callApi(rawResult.text)
        supportFragmentManager.popBackStack()
    }

    private fun setCheckedInNav(index: Int) {
        nav_view.menu.getItem(index).isChecked = true
    }

    private fun callApi(numberCode: String) {
        val observerCheckin = object : SimpleSubscribe<ResponseBody>() {
            override fun onCompleted(success: Boolean, value: ResponseBody?, error: BaseException?) {
                super.onCompleted(success, value, error)
                cancelLoading()
                when {
                    success && value != null -> responseCheckIn(value)
                    else -> handleExceptionOnRestApi(error!! as RestApiException)
                }
            }
        }
        val observerScanGift = object : SimpleSubscribe<ScanResponse>() {
            override fun onCompleted(success: Boolean, value: ScanResponse?, error: BaseException?) {
                super.onCompleted(success, value, error)
                cancelLoading()
                when {
                    success && value != null -> reponseGiftCode(value)
                    else -> handleExceptionOnRestApi(error!! as RestApiException)
                }
            }
        }
        eventId?.let {
            if (scanGiftCode) scanRepository.checkGiftCode(numberCode, eventId!!).subscribe(observerScanGift)
            else scanRepository.check_in(numberCode, eventId!!).subscribe(observerCheckin)
        }
    }

    private fun responseCheckIn(value: ResponseBody) {
        var textResponse = "${value.string()}"
        Log.d("TAG", "text: $textResponse")
        if (textResponse == MessageCode.CHECK_IN_SUCCESSFUL) homePresenter.refreshView(getStringResource(R.string.string_check_in_success))
        else homePresenter.refreshView("Some problem with server!")
    }

    private fun reponseGiftCode(value: ScanResponse) {
        homePresenter.refreshView("Congratulation ${value.name}.")
    }

    private fun handleExceptionOnRestApi(restApiException: RestApiException) {
        restApiException.onError(object : RestApiException.CallBackError {
            override fun onUnAuthorize() {

            }

            override fun onBadRequest() {

            }

            override fun onServerError() {

            }

            override fun onForBidden() {

            }

            override fun onNoRoute() {

            }

            override fun connectException() {
                detectInternetState()
            }
        })
        restApiException.message?.let { showErrorByToast(it) }
    }


    private fun showErrorByToast(error: String) {
        Log.e("TAG", "error: $error")
        homePresenter.showInformationCheckin(error)
        Toast.makeText(appContext, error, Toast.LENGTH_SHORT).show()
    }

    fun showLoading() {
        if (!loaderDialog.isShowing) {
            loaderDialog.show()
        }
    }

    fun cancelLoading() {
        if (loaderDialog.isShowing) {
            loaderDialog.dismiss()
        }
    }

    private fun enableViews(enable: Boolean) {
        if (enable) {
            toggle.isDrawerIndicatorEnabled = false
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            if (!mToolBarNavigationListenerIsRegistered) {
                toggle.toolbarNavigationClickListener = View.OnClickListener {
                    supportFragmentManager.popBackStack()
                    enableViews(false)
                }
                mToolBarNavigationListenerIsRegistered = true
            }

        } else {
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            toggle.isDrawerIndicatorEnabled = true
            toggle.toolbarNavigationClickListener = null
            mToolBarNavigationListenerIsRegistered = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelLoading()
    }
}
