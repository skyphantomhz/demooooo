package com.axonactive.eventomorrowsupporter.feature.main.analysis


class AnalysisPresenter(val view: AnalysisContract.View): AnalysisContract.Presenter {
    init {
        view.presenter = this
    }
}