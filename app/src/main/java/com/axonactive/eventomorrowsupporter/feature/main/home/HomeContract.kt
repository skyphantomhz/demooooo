package com.axonactive.eventomorrowsupporter.feature.main.home

import com.axonactive.eventomorrowsupporter.base.BaseView

interface HomeContract {
    interface View : BaseView<Presenter> {
        fun showErrorByToast(error: String)
        fun showInformationCheckin(error: String)
        fun switchToScan()
        fun showResponse(text: String)
    }

    interface Presenter {
        fun switchToScan()
        fun refreshView(value: String)
        fun showInformationCheckin(error: String)
    }
}