package com.axonactive.eventomorrowsupporter.feature.main.scanner

import com.axonactive.eventomorrowsupporter.base.BaseView
import com.google.zxing.Result

interface ScannerContract {
    interface View : BaseView<Presenter> {
        fun showToast(rawResult: Result)
        fun switchToHome(rawResult: Result)
    }

    interface Presenter {
        fun handleResultQrCode(rawResult: Result?)
    }
}