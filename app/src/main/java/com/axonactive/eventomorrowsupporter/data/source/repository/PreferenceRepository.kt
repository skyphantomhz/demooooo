package com.axonactive.eventomorrowsupporter.data.source.repository

import com.axonactive.eventomorrowsupporter.data.source.PreferenceDataSource

interface PreferenceRepository {
    fun isLoginByMode(loginMode: PreferenceDataSource.LOGIN_MODE): Boolean

    fun saveLoginMode(loginMode: PreferenceDataSource.LOGIN_MODE)

    fun saveDeviceToken(token: String)

    fun getDeviceToken(): String

    fun saveNewDeviceToken(token: String)

    fun getNewDeviceToken(): String

    fun saveNotificationId(id: Int)

    fun getNotificationId(): List<Int>
}