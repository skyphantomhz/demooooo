package com.axonactive.eventomorrowsupporter.data.source.remote

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.request.AccessToken
import com.axonactive.eventomorrowsupporter.data.model.request.SignIn
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import com.axonactive.eventomorrowsupporter.data.service.AuthenticationService
import com.axonactive.eventomorrowsupporter.data.source.repository.UserRepository
import com.axonactive.eventomorrowsupporter.extensions.RxUtils
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

class UserRemoteSource @Inject constructor(var localSource: UserRepository,
                                           @Named("auth-basic") var authenticationService: AuthenticationService) : UserRepository {
    override fun clearData() {

    }

    override fun getRefreshToken(): String {
        //Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun loginByEmail(email: String, password: String): Observable<AccessTokenResponse> =
            authenticationService
                    .getToken(SignIn(email = email, password = password))
                    .doOnNext {
                        localSource.saveAccessToken(it.access_token)
                        localSource.saveRefreshToken(it.refresh_token)
                    }
                    .compose(RxUtils.applySchedulers())


    override fun loginByFacebook(token: String): Observable<AccessTokenResponse> =
            authenticationService
                    .loginFBAccount(AccessToken(token))
                    .doOnNext {
                        localSource.saveAccessToken(it.access_token)
                        localSource.saveRefreshToken(it.refresh_token)
                    }
                    .compose(RxUtils.applySchedulers())

    override fun loginByGoogle(token: String): Observable<AccessTokenResponse> =
            authenticationService
                    .loginGGAccount(AccessToken(token))
                    .doOnNext {
                        localSource.saveAccessToken(it.access_token)
                        localSource.saveRefreshToken(it.refresh_token)
                    }
                    .compose(RxUtils.applySchedulers())

    override fun saveRefreshToken(token: String) {
        //Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun saveAccessToken(token: String) {
        //Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun isTokenEmpty(): Boolean {
        //Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun saveUserProfile(profile: UserProfile?) {

    }

    override fun saveTicketNumber(code: String) {
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun getTicketNumber(): String {
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun saveEventId(id: Long) {
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun getEventId(): Long {
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun getAccessToken(): String {
        return ""
    }

//    override fun fetchUserProfile(): Observable<UserProfile> {
//        return authenticationServiceBearer.getProfile().doOnNext { t: UserProfile ->
//            localSource.saveUserProfile(t)
//        }.compose(RxUtils.applySchedulers())
//    }

    override fun getUserProfile(): UserProfile? {
        return null
    }
}