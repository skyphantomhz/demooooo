package com.axonactive.eventomorrowsupporter.data.source

import com.axonactive.eventomorrowsupporter.data.model.ScanResponse
import com.axonactive.eventomorrowsupporter.data.source.repository.ScanRepository
import io.reactivex.Observable
import okhttp3.ResponseBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScanDataSource @Inject constructor(var scanRemoteSource: ScanRepository) : ScanRepository {

    override fun check_in(ticketCode: String, eventId: Int): Observable<ResponseBody>
            = this.scanRemoteSource.check_in(ticketCode, eventId)

    override fun checkGiftCode(numberCode: String, eventId: Int): Observable<ScanResponse>
            = this.scanRemoteSource.checkGiftCode(numberCode, eventId)
}