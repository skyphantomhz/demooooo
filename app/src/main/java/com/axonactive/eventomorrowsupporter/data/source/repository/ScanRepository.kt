package com.axonactive.eventomorrowsupporter.data.source.repository

import com.axonactive.eventomorrowsupporter.data.model.ScanResponse
import io.reactivex.Observable
import okhttp3.ResponseBody

interface ScanRepository {
    fun check_in(ticketCode: String, eventId: Int): Observable<ResponseBody>
    fun checkGiftCode(numberCode: String, eventId: Int): Observable<ScanResponse>
}