package com.axonactive.eventomorrowsupporter.data.source.remote

import com.axonactive.eventomorrowsupporter.data.model.response.EventResponse
import com.axonactive.eventomorrowsupporter.data.service.EventService
import com.axonactive.eventomorrowsupporter.data.source.repository.EventRepository
import com.axonactive.eventomorrowsupporter.extensions.RxUtils
import io.reactivex.Observable
import javax.inject.Inject

class EventRemoteSource @Inject constructor(var eventService: EventService) : EventRepository {
    override fun getUpComingEvents(pageNumber: Int, pageSize: Int): Observable<EventResponse>
            = eventService.getUpComingEvents(pageNumber, pageSize).compose(RxUtils.applySchedulers())
}