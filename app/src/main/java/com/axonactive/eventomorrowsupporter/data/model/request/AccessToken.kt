package com.axonactive.eventomorrowsupporter.data.model.request

import com.google.gson.annotations.SerializedName

class AccessToken(@SerializedName("accessToken") var accessToken: String)