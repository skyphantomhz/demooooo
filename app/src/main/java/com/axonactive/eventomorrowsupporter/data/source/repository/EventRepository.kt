package com.axonactive.eventomorrowsupporter.data.source.repository

import com.axonactive.eventomorrowsupporter.data.model.response.EventResponse
import io.reactivex.Observable

interface EventRepository {
    fun getUpComingEvents(pageNumber: Int, pageSize: Int): Observable<EventResponse>
}