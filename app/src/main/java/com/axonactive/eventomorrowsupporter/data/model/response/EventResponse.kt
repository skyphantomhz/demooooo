package com.axonactive.eventomorrowsupporter.data.model.response

import com.axonactive.eventomorrowsupporter.data.model.Event
import com.google.gson.annotations.SerializedName

class EventResponse(

        @SerializedName("responseList")
        var listEvents: List<Event>? = null,

        @SerializedName("isLastPage")
        var isLastPage: Boolean? = true
)