package com.axonactive.eventomorrowsupporter.data.model.response

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.google.gson.annotations.SerializedName

class TokenResponse(
        @SerializedName("profile") var profile: UserProfile,

        @SerializedName("token") var token: String
)