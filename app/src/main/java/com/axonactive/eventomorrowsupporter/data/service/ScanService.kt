package com.axonactive.eventomorrowsupporter.data.service

import com.axonactive.eventomorrowsupporter.data.model.ScanResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.POST
import retrofit2.http.Query

interface ScanService {
    @POST("/api/tickets/support/check-in")
    fun check_in(@Query("ticket-code") ticketCode: String, @Query("event-id") eventId: Int): Observable<ResponseBody>

    @POST("/api/tickets/support/scan-gift")
    fun checkGiftCode(@Query("gift-code") giftCode: String, @Query("event-id") eventId: Int): Observable<ScanResponse>
}