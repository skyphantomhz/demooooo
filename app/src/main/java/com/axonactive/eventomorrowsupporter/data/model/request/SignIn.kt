package com.axonactive.eventomorrowsupporter.data.model.request

import com.google.gson.annotations.SerializedName


class SignIn(@SerializedName("email")
             var email: String? = null,

             @SerializedName("password")
             var password: String? = null)