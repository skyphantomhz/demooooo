package com.axonactive.eventomorrowsupporter.data.source.repository

import com.axonactive.eventomorrowsupporter.feature.CallBackUpdateView

interface AnalysisRepository {
    fun setListenerUpdateView(callback: CallBackUpdateView)
    fun remoteListenerUpdateView()
    fun setEventId(id: Int)
    fun participantCount(): Int
}