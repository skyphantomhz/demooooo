package com.axonactive.eventomorrowsupporter.data.source

import com.axonactive.eventomorrowsupporter.data.source.repository.AnalysisRepository
import com.axonactive.eventomorrowsupporter.feature.CallBackUpdateView
import javax.inject.Inject

class AnalysisDataSource @Inject constructor(var analysisRemoteSource: AnalysisRepository) : AnalysisRepository {
    override fun setListenerUpdateView(callback: CallBackUpdateView) {
        analysisRemoteSource.setListenerUpdateView(callback)
    }

    override fun remoteListenerUpdateView() = analysisRemoteSource.remoteListenerUpdateView()

    override fun setEventId(id: Int) = analysisRemoteSource.setEventId(id)

    override fun participantCount() = analysisRemoteSource.participantCount()
}