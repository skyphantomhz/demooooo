package com.axonactive.eventomorrowsupporter.data.source.remote

import com.axonactive.eventomorrowsupporter.data.AnalyticsStatus.CHECKED_IN
import com.axonactive.eventomorrowsupporter.data.AnalyticsStatus.CONFIRMED
import com.axonactive.eventomorrowsupporter.data.AnalyticsStatus.DECLINED
import com.axonactive.eventomorrowsupporter.data.AnalyticsStatus.UNCONFIRMED
import com.axonactive.eventomorrowsupporter.data.model.Participant
import com.axonactive.eventomorrowsupporter.data.source.repository.AnalysisRepository
import com.axonactive.eventomorrowsupporter.feature.CallBackUpdateView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import javax.inject.Inject

class AnalysisRemoteSource @Inject constructor(val databaseReference: DatabaseReference) : AnalysisRepository {

    private var callBackUpdateView: CallBackUpdateView? = null
    private var database: DatabaseReference? = null
    private var participantCount: Int = 0

    private val listener = object : ValueEventListener {
        override fun onCancelled(databaseError: DatabaseError) {
            callBackUpdateView?.onCancelledFirebase(databaseError)
        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            callBackUpdateView?.let {
                analysisData(dataSnapshot)
            }
        }
    }

    override fun setListenerUpdateView(callback: CallBackUpdateView) {
        this.callBackUpdateView = callback
    }

    private fun analysisData(dataSnapshot: DataSnapshot) {
        var unConfirmedCount = 0
        var confirmedCount = 0
        var checkInCount = 0
        var declinedCount = 0
        participantCount = 0
        dataSnapshot.children.forEach {
            val participant = it.getValue(Participant::class.java)
            participant?.let {
                when (participant.status) {
                    UNCONFIRMED -> unConfirmedCount += 1
                    CONFIRMED -> confirmedCount += 1
                    CHECKED_IN -> checkInCount += 1
                    DECLINED -> declinedCount += 1
                }
            }
            participantCount+=1
        }
        callBackUpdateView?.updateView(unConfirmedCount, confirmedCount, checkInCount, declinedCount)
    }

    private fun initListenerDatabase() {
        database?.addValueEventListener(listener)
    }

    override fun remoteListenerUpdateView() {
        database?.removeEventListener(listener)
    }

    override fun setEventId(id: Int) {
        remoteListenerUpdateView()
        database = databaseReference.child(id.toString()).child("participants")
        initListenerDatabase()
    }

    override fun participantCount() = participantCount
}