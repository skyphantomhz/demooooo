package com.axonactive.eventomorrowsupporter.data.source.local

import android.content.SharedPreferences
import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import com.axonactive.eventomorrowsupporter.data.source.repository.UserRepository
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import io.reactivex.Observable
import javax.inject.Inject


class UserLocalSource @Inject constructor(var sharedPreferences: SharedPreferences) : UserRepository {


    override fun loginByEmail(email: String, password: String): Observable<AccessTokenResponse> {
        // Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun loginByFacebook(token: String): Observable<AccessTokenResponse> {
        // Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun loginByGoogle(token: String): Observable<AccessTokenResponse> {
        // Not required
        throw NotImplementedError("An operation is not implemented: Not required ")
    }

    override fun saveRefreshToken(token: String) {
        sharedPreferences.edit()
                .putString(PREFERENCE_REFRESH_TOKEN, token)
                .apply()
    }

    override fun saveTicketNumber(code: String) {
        sharedPreferences.edit()
                .putString(PREFERENCE_CODE, code)
                .apply()
    }

    override fun getTicketNumber(): String {
        return sharedPreferences.getString(PREFERENCE_CODE, "")
    }

    override fun saveAccessToken(token: String) {
        sharedPreferences.edit()
                .putString(PREFERENCE_ACCESS_TOKEN, token)
                .apply()
    }

    override fun saveUserProfile(profile: UserProfile?) {
        if (profile != null) {
            val json = Gson().toJson(profile)
            sharedPreferences.edit()
                    .putString(PREFERENCE_USER_PROFILE, json)
                    .apply()
        } else {
            sharedPreferences.edit()
                    .putString(PREFERENCE_USER_PROFILE, DEFAULT_VALUE)
                    .apply()
        }

    }

    override fun saveEventId(id: Long) {
        sharedPreferences.edit().putLong(PREFERENCE_EVENT_ID, id).apply()
    }

    override fun getEventId(): Long {
        return sharedPreferences.getLong(PREFERENCE_EVENT_ID, 0)
    }

    override fun isTokenEmpty(): Boolean {
        val token = sharedPreferences.getString(PREFERENCE_ACCESS_TOKEN, DEFAULT_VALUE)
        return token.isEmpty()
    }

    override fun getAccessToken(): String {
        return sharedPreferences.getString(PREFERENCE_ACCESS_TOKEN, DEFAULT_VALUE)
    }

    override fun getRefreshToken(): String {
        return sharedPreferences.getString(PREFERENCE_REFRESH_TOKEN, DEFAULT_VALUE)
    }

    override fun getUserProfile(): UserProfile? {
        val json = sharedPreferences.getString(PREFERENCE_USER_PROFILE, DEFAULT_VALUE)
        try {
            val userProfile = Gson().fromJson<UserProfile>(json, UserProfile::class.java)
            return userProfile
        } catch (e: JsonSyntaxException) {
            return null
        }
    }

    override fun clearData() {
        sharedPreferences.edit()
                .putString(PREFERENCE_REFRESH_TOKEN, DEFAULT_VALUE)
                .putString(PREFERENCE_ACCESS_TOKEN, DEFAULT_VALUE)
                .apply()
        saveUserProfile(null)
    }

    companion object {
        private const val PREFERENCE_NAME = "UserSharedPreference"
        private const val PREFERENCE_REFRESH_TOKEN = "RefreshToken"
        const val PREFERENCE_ACCESS_TOKEN = "AccessToken"
        private const val PREFERENCE_USER_PROFILE = "UserProfile"
        private const val PREFERENCE_CODE = "Code"
        private const val PREFERENCE_EVENT_ID = "EventId"
        const val DEFAULT_VALUE = ""
    }
}