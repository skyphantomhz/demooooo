package com.axonactive.eventomorrowsupporter.data.model.response

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.google.gson.annotations.SerializedName

class ProfileResponse(
        @SerializedName("token")
        var token: String,

        @SerializedName("profile")
        var userProfile: UserProfile,

        @SerializedName("message")
        var message: String? = null
)