package com.axonactive.eventomorrowsupporter.data.model.response

import com.google.gson.annotations.SerializedName

class AccessTokenResponse(@SerializedName("access_token") val access_token: String,
                          @SerializedName("token_type") val token_type: String,
                          @SerializedName("refresh_token") val refresh_token: String,
                          @SerializedName("expires_in") val expires_in: Int,
                          @SerializedName("scope") val scope: String
)