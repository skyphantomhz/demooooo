package com.axonactive.eventomorrowsupporter.data.model

import com.google.gson.annotations.SerializedName

class UserProfile(

        @SerializedName("id")
        var id: Long,

        @SerializedName("lastUpdatedDate")
        var lastUpdatedDate: String? = null,

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("company")
        var company: String? = null,

        @SerializedName("address")
        var address: String? = null,

        @SerializedName("phoneNumber")
        var phoneNumber: String? = null,

        @SerializedName("birthday")
        var birthday: String? = null,

        @SerializedName("gender")
        var gender: String? = null,

        @SerializedName("job")
        var job: String? = null,

        @SerializedName("avatar")
        var avatar: String? = null,

        @SerializedName("status")
        var status: String? = null,

        @SerializedName("accountType")
        var accountType: String? = null,

        @SerializedName("profileId")
        var profileId: Long? = null,

        @SerializedName("enabled")
        var enabled: Boolean? = null,

        @SerializedName("lastPasswordResetDate")
        var lastPasswordResetDate: String? = null
)