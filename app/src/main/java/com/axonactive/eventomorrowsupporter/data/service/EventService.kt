package com.axonactive.eventomorrowsupporter.data.service

import com.axonactive.eventomorrowsupporter.data.model.response.EventResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface EventService {
    @GET("/api/events/supported-paging")
    fun getUpComingEvents(@Query("page-number") pageNumber: Int, @Query("page-size") pageSize: Int,
                          @Query("sort-key") sortKey: String = ""): Observable<EventResponse>
}