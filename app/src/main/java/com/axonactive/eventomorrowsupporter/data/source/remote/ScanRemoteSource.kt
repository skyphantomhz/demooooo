package com.axonactive.eventomorrowsupporter.data.source.remote

import com.axonactive.eventomorrowsupporter.data.model.ScanResponse
import com.axonactive.eventomorrowsupporter.data.service.ScanService
import com.axonactive.eventomorrowsupporter.data.source.repository.ScanRepository
import com.axonactive.eventomorrowsupporter.extensions.RxUtils
import io.reactivex.Observable
import okhttp3.ResponseBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScanRemoteSource @Inject constructor(var scanService: ScanService)
    : ScanRepository {

    override fun check_in(ticketCode: String, eventId: Int): Observable<ResponseBody> {
        return scanService.check_in(ticketCode, eventId).compose(RxUtils.applySchedulers())
    }

    override fun checkGiftCode(numberCode: String, eventId: Int): Observable<ScanResponse> {
        return scanService.checkGiftCode(numberCode, eventId).compose(RxUtils.applySchedulers())
    }
}