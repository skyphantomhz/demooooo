package com.axonactive.eventomorrowsupporter.data

import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.getStringResource

object AnalyticsStatus {
    const val UNCONFIRMED = "UNCONFIRMED"
    const val CONFIRMED = "CONFIRMED"
    const val CHECKED_IN = "CHECKED_IN"
    const val DECLINED = "DECLINED"

    val status = arrayOf(getStringResource(R.string.string_confirmed),
            getStringResource(R.string.string_unconfirmed),
            getStringResource(R.string.string_declined),
            getStringResource(R.string.string_check_in))
}