package com.axonactive.eventomorrowsupporter.data.source

import com.axonactive.eventomorrowsupporter.data.model.response.EventResponse
import com.axonactive.eventomorrowsupporter.data.source.remote.EventRemoteSource
import com.axonactive.eventomorrowsupporter.data.source.repository.EventRepository
import io.reactivex.Observable
import javax.inject.Inject

class EventDataSource @Inject constructor(var eventRemoteSource: EventRemoteSource) : EventRepository {
    override fun getUpComingEvents(pageNumber: Int, pageSize: Int): Observable<EventResponse> {
        return eventRemoteSource.getUpComingEvents(pageNumber, pageSize)
    }
}