package com.axonactive.eventomorrowsupporter.data.model

import com.google.gson.annotations.SerializedName


data class Participant constructor(
        @SerializedName("birthdayISO")
        var birthdayISO: String? = null,
        @SerializedName("checkInType")
        var checkInType: String? = null,
        @SerializedName("checkedInTime")
        var checkedInTime: CheckedInTime? = null,
        @SerializedName("code")
        var code: String,
        @SerializedName("email")
        var email: String,
        @SerializedName("eventId")
        var eventId: Int,
        @SerializedName("gender")
        var gender: String? = null,
        @SerializedName("id")
        var id: Int,
        @SerializedName("job")
        var job: String? = null,
        @SerializedName("name")
        var name: String,
        @SerializedName("phoneNumber")
        var phoneNumber: String? = null,
        @SerializedName("previousStatus")
        var previousStatus: String? = null,
        @SerializedName("qrCodeImage")
        var qrCodeImage: String,
        @SerializedName("receiveGift")
        var receiveGift: Boolean? = false,
        @SerializedName("status")
        var status: String,
        @SerializedName("supporter")
        var supporter: Boolean? = false,
        @SerializedName("userId")
        var userId: Int,
        @SerializedName("company")
        var company: String? = null,
        @SerializedName("giftCode")
        var giftCode: String? = null

){
        constructor() : this(null,null,null,"","",0,null,0,null,"",null,null,"",false,"",false,0,null)
}