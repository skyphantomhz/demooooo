package com.axonactive.eventomorrowsupporter.data.model.response

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.google.gson.annotations.SerializedName

class SocialProfileResponse(
        @SerializedName("profile") var userProfile: UserProfile,

        @SerializedName("token") var token: TokenResponse
)