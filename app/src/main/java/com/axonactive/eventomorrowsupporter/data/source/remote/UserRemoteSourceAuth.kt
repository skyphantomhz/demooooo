package com.axonactive.eventomorrowsupporter.data.source.remote

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.service.AuthenticationService
import com.axonactive.eventomorrowsupporter.data.source.repository.UserRepository
import com.axonactive.eventomorrowsupporter.extensions.RxUtils
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

class UserRemoteSourceAuth @Inject constructor(var localSource: UserRepository,
                                           @Named("auth-bearer") var authenticationServiceBearer: AuthenticationService)  {
    fun fetchUserProfile(): Observable<UserProfile> {
        return authenticationServiceBearer.getProfile().doOnNext { t: UserProfile ->
            localSource.saveUserProfile(t)
        }.compose(RxUtils.applySchedulers())
    }
}