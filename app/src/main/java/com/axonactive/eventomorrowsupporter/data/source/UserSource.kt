package com.axonactive.eventomorrowsupporter.data.source

import android.util.Log
import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import com.axonactive.eventomorrowsupporter.data.source.local.UserLocalSource
import com.axonactive.eventomorrowsupporter.data.source.remote.UserRemoteSource
import com.axonactive.eventomorrowsupporter.data.source.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class UserSource @Inject constructor(var remoteSource: UserRemoteSource, var localSource: UserLocalSource) : UserRepository {
    override fun saveTicketNumber(code: String) {
        return localSource.saveTicketNumber(code)
    }

    override fun getTicketNumber(): String {
        return localSource.getTicketNumber()
    }

    override fun saveEventId(id: Long) {
        return localSource.saveEventId(id)
    }

    override fun getEventId(): Long {
        return localSource.getEventId()
    }

    override fun clearData() {
        localSource.clearData()
    }

    override fun saveRefreshToken(token: String) {
        localSource.saveRefreshToken(token)
    }

    override fun getRefreshToken(): String {
        return localSource.getRefreshToken()
    }

    override fun loginByEmail(email: String, password: String): Observable<AccessTokenResponse> {
        return remoteSource.loginByEmail(email, password)
    }

    override fun loginByFacebook(token: String): Observable<AccessTokenResponse> {
        return remoteSource.loginByFacebook(token)
    }

    override fun loginByGoogle(token: String): Observable<AccessTokenResponse> {
        return remoteSource.loginByGoogle(token)
    }


    override fun saveAccessToken(token: String) {
        localSource.saveAccessToken(token)
    }

    override fun isTokenEmpty(): Boolean {
        return localSource.isTokenEmpty()
    }

    override fun getAccessToken(): String {
        Log.e("TAG", "token is: ${localSource.getAccessToken()}")
        return localSource.getAccessToken()
    }

    override fun saveUserProfile(profile: UserProfile?) {
        localSource.saveUserProfile(profile)
        remoteSource.saveUserProfile(profile)
    }

    override fun getUserProfile(): UserProfile? {
        return localSource.getUserProfile()
    }
}