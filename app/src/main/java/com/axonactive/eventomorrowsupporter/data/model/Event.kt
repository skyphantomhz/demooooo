package com.axonactive.eventomorrowsupporter.data.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Event(

        @SerializedName("id")
        var id: Int,

        @SerializedName("title")
        var title: String

) : Parcelable{
        override fun toString(): String {
                return title
        }
}