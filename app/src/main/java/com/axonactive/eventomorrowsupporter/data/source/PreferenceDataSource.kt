package com.axonactive.eventomorrowsupporter.data.source

import android.content.SharedPreferences
import com.axonactive.eventomorrowsupporter.data.source.repository.PreferenceRepository
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import javax.inject.Inject

class PreferenceDataSource @Inject constructor(var sharedPreferences: SharedPreferences) : PreferenceRepository {
    enum class LOGIN_MODE {
        FACEBOOK, NORMAL, GOOGLE, SKIP
    }

    override fun saveDeviceToken(token: String) {
        sharedPreferences
                .edit()
                .putString(DEVICE_TOKEN, token)
                .apply()
    }

    override fun getDeviceToken(): String {
        return sharedPreferences.getString(DEVICE_TOKEN, FirebaseInstanceId.getInstance().token)
    }

    override fun isLoginByMode(loginMode: LOGIN_MODE): Boolean {
        val loginType = sharedPreferences.getString(LOGIN_TYPE, NORMAL_TYPE)
        return when (loginMode) {
            LOGIN_MODE.FACEBOOK -> loginType.equals(FACEBOOK_TYPE)
            LOGIN_MODE.NORMAL -> loginType.equals(NORMAL_TYPE)
            LOGIN_MODE.GOOGLE -> loginType.equals(GOOGLE_TYPE)
            LOGIN_MODE.SKIP -> loginType.equals(NO_LOGIN)
        }
    }

    override fun saveLoginMode(loginMode: LOGIN_MODE) {
        val editor = sharedPreferences.edit()
        when (loginMode) {
            LOGIN_MODE.FACEBOOK -> editor.putString(LOGIN_TYPE, FACEBOOK_TYPE)
            LOGIN_MODE.NORMAL -> editor.putString(LOGIN_TYPE, NORMAL_TYPE)
            LOGIN_MODE.GOOGLE -> editor.putString(LOGIN_TYPE, GOOGLE_TYPE)
            LOGIN_MODE.SKIP -> editor.putString(LOGIN_TYPE, NO_LOGIN)
        }
        editor.apply()
    }

    override fun saveNewDeviceToken(token: String) {
        sharedPreferences
                .edit()
                .putString(NEW_DEVICE_TOKEN, token)
                .apply()
    }

    override fun getNewDeviceToken(): String {
        return sharedPreferences
                .getString(NEW_DEVICE_TOKEN, DEFAULT_EMPTY)
    }

    override fun saveNotificationId(id: Int) {
        val list: ArrayList<Int>
        if (id == DEFAULT_ARRAY_EMPTY) {
            list = ArrayList()
        } else {
            list = getNotificationId()
            if (!list.contains(id))
                list.add(id)
        }
        val jsonString = Gson().toJson(list)
        sharedPreferences
                .edit()
                .putString(NOTIFICATION, jsonString)
                .apply()
    }

    override fun getNotificationId(): ArrayList<Int> {
        val type = object : TypeToken<ArrayList<Int>>() {}.type
        val gson = Gson()
        val jsonString = sharedPreferences.getString(NOTIFICATION, "[]")
        return gson.fromJson(jsonString, type)
    }

    companion object {
        const val PREFERENCE_NAME = "SharedPreference"
        private const val DEVICE_TOKEN = "DeviceToken"
        private const val NEW_DEVICE_TOKEN = "NewDeviceToken"
        private const val LOGIN_TYPE = "LoginTYPE"
        private const val FACEBOOK_TYPE = "Facebook"
        private const val NORMAL_TYPE = "Normal"
        private const val GOOGLE_TYPE = "Google"
        private const val NO_LOGIN = "NoLogin"
        private const val NOTIFICATION = "notification"
        const val DEFAULT_EMPTY = ""
        const val DEFAULT_ARRAY_EMPTY = -1
    }

}