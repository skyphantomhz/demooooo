package com.axonactive.eventomorrowsupporter.data.model

import com.google.gson.annotations.SerializedName

class CheckedInTime constructor(
        @SerializedName("date")
        var date: Int,
        @SerializedName("day")
        var day: Int,
        @SerializedName("hours")
        var hours: Int,
        @SerializedName("minutes")
        var minutes: Int,
        @SerializedName("month")
        var month: Int,
        @SerializedName("seconds")
        var seconds: Int,
        @SerializedName("time")
        var time: Long,
        @SerializedName("timezoneOffset")
        var timezoneOffset: Int,
        @SerializedName("year")
        var year: Int
){
        constructor(): this(0,0,0,0,0,0,0,0,0)
}