package com.axonactive.eventomorrowsupporter.data.source.repository


import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import io.reactivex.Observable

interface UserRepository {
    fun loginByEmail(email: String, password: String): Observable<AccessTokenResponse>

    fun loginByFacebook(token: String): Observable<AccessTokenResponse>

    fun loginByGoogle(token: String): Observable<AccessTokenResponse>

    fun saveAccessToken(token: String)

    fun saveUserProfile(profile: UserProfile?)

    fun isTokenEmpty(): Boolean

    fun getAccessToken(): String

//    fun fetchUserProfile() : Observable<UserProfile>

    fun getUserProfile(): UserProfile?

    fun getRefreshToken(): String

    fun saveRefreshToken(token: String)

    fun saveTicketNumber(code: String)

    fun getTicketNumber(): String

    fun saveEventId(id: Long)

    fun getEventId(): Long

    fun clearData()
}