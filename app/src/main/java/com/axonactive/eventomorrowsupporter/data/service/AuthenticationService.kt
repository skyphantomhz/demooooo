package com.axonactive.eventomorrowsupporter.data.service

import com.axonactive.eventomorrowsupporter.data.model.UserProfile
import com.axonactive.eventomorrowsupporter.data.model.request.AccessToken
import com.axonactive.eventomorrowsupporter.data.model.request.SignIn
import com.axonactive.eventomorrowsupporter.data.model.response.AccessTokenResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthenticationService {

    @POST("/auth/login")
    fun loginByAccount(@Body signIn: SignIn): Observable<AccessTokenResponse>

    @POST("/auth/social/facebook")
    fun loginFBAccount(@Body accessToken: AccessToken): Observable<AccessTokenResponse>

    @POST("/auth/social/google")
    fun loginGGAccount(@Body accessToken: AccessToken): Observable<AccessTokenResponse>

    @POST("/auth/forgot-password")
    fun resetPassword(@Query("email") email: String): Observable<ResponseBody>

    @POST("/auth/token")
    fun getToken(@Body account: SignIn,
                 @Query("grant_type") grant_type: String = "password",
                 @Query("scope") scope: String = "read write"
    ): Observable<AccessTokenResponse>

    @GET("/auth/token")
    fun refreshToken(@Query("refresh_token") refreshToken: String,
                     @Query("grant_type") grant_type: String = "refresh_token",
                     @Query("scope") scope: String = "read write"
    ): Call<AccessTokenResponse>

    @GET("/api/users/get")
    fun getProfile(): Observable<UserProfile>
}