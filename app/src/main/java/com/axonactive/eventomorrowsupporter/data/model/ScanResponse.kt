package com.axonactive.eventomorrowsupporter.data.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class ScanResponse(
        @SerializedName("code")
        val code: String? = null,

        @SerializedName("email")
        val email: String? = null,

        @SerializedName("eventTitle")
        val eventTitle: String? = null,

        @SerializedName("id")
        val id: Long,

        @SerializedName("name")
        val name: String? = null,

        @SerializedName("phoneNumber")
        val phoneNumber: String? = null,

        @SerializedName("qrCodeImageUrl")
        val qrCodeImageUrl: String? = null,

        @SerializedName("startTime")
        val startTime: Long? = null,

        @SerializedName("venue")
        val venue: String? = null
) : Parcelable