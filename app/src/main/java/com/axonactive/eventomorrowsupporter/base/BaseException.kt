package com.axonactive.eventomorrowsupporter.base

interface BaseException {
    var error: Throwable
    var message: String?
    fun onError(baseCallBack: BaseCallBack)
}

interface BaseCallBack {}
