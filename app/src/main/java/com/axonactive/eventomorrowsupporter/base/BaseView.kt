package com.axonactive.eventomorrowsupporter.base

interface BaseView<T> {
    var presenter: T
}
