package com.axonactive.eventomorrowsupporter.base

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.axonactive.eventomorrowsupporter.R
import com.axonactive.eventomorrowsupporter.extensions.isInternetAvailable
import com.axonactive.eventomorrowsupporter.extensions.showDialogOnlyAccept
import com.axonactive.eventomorrowsupporter.getStringResource

abstract class BaseActivity : AppCompatActivity() {
    private var doubleBackToExitPressedOnce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResourceId())
    }

    protected abstract fun getLayoutResourceId(): Int

    override fun onResume() {
        super.onResume()
        detectInternetState()
    }

    fun detectInternetState() {
        if (!this.isInternetAvailable()) {
            this.showDialogOnlyAccept(this, resources.getString(R.string.string_title_no_internet), resources.getString(R.string.string_content_no_internet))
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish()
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, getStringResource(R.string.string_message_double_back_to_exit), Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}